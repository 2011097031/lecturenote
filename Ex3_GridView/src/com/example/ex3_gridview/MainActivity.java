package com.example.ex3_gridview;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
        GridView grid = (GridView)findViewById(R.id.gridView1);
        grid.setAdapter(new GridAdapter(this));

	}

    public class GridAdapter extends BaseAdapter {
        private Context mContext;
    
        private Integer[] mImageId = {
                R.drawable.logo1,
                R.drawable.logo2,
                R.drawable.logo3,
                R.drawable.logo4,
                R.drawable.logo1,
                R.drawable.logo2,
                R.drawable.logo3,
                R.drawable.logo4,
                R.drawable.logo1,
                R.drawable.logo2,
                R.drawable.logo3,
                R.drawable.logo4,
                R.drawable.logo1,
                R.drawable.logo2,
                R.drawable.logo3,
                R.drawable.logo4,
                R.drawable.logo1,
                R.drawable.logo2,
                R.drawable.logo3,
                R.drawable.logo4
        };
    
        public GridAdapter(Context c) {
            mContext = c;
        }
    
        public View getView(int pos, View v, ViewGroup p) {
            ImageView img;
    
            if (v == null) {
                img = new ImageView(mContext);
                img.setLayoutParams(new GridView.LayoutParams(75,75));
                img.setAdjustViewBounds(false);
                img.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
            else {
                img = (ImageView)v;
            }
    
            img.setImageResource(mImageId[pos]);

            return img;
        }
    
        public int getCount() {
            return mImageId.length;
        }
    
        public Object getItem(int pos) {
            return pos;
        }
    
        public long getItemId(int pos) {
            return pos;
        }
    }
}


