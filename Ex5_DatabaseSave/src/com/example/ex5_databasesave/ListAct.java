package com.example.ex5_databasesave;

import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.widget.ListAdapter;

public class ListAct extends ListActivity {

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list);
        loadDB();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadDB();
    }
    public void loadDB() {
        SQLiteDatabase db = openOrCreateDatabase(
            "test.db",
            SQLiteDatabase.CREATE_IF_NECESSARY,
            null
            );
        db.execSQL("CREATE TABLE IF NOT EXISTS people " +
            "(_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, age INTEGER);");
    
        Cursor c = db.rawQuery("SELECT * FROM people;", null);
        startManagingCursor(c);
    
        ListAdapter adapt = new SimpleCursorAdapter(
            this,
            android.R.layout.simple_list_item_2,
            c,
            new String[] {"name","age"},
            new int[] {android.R.id.text1,android.R.id.text2}
            );
        setListAdapter(adapt);
    
        if (db != null) {
            db.close();
        }
    }
}


	

