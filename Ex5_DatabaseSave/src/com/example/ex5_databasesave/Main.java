package com.example.ex5_databasesave;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;


public class Main extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    public void showListAct(View v) {
        Intent i = new Intent(this, ListAct.class);
        startActivity(i);
    }

    public void showInsAct(View v) {
        Intent i = new Intent(this, InsAct.class);
        startActivity(i);
    }
    /*
    public void showDelAct(View v) {
        Intent i = new Intent(this, DelAct.class);
        startActivity(i);
    }

    public void showSqlAct(View v) {
        Intent i = new Intent(this, SqlAct.class);
        startActivity(i);
    }
	*/
   
}
