package com.example.ex3_tabactivity;

import android.app.TabActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.Toast;

public class MainActivity extends TabActivity {
	
	WebView webview ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	
        TabHost host = getTabHost();

        host.addTab(host.newTabSpec("Tab1")
        .setIndicator("GOOGLE")
        .setContent(R.id.first));

        host.addTab(host.newTabSpec("Tab2")
        	
            .setIndicator("NAVER")
            .setContent(R.id.second) );

        host.addTab(host.newTabSpec("Tab3")
            .setIndicator("DAUM")
            .setContent(R.id.third) );
        
        host.addTab(host.newTabSpec("Tab4")
                .setIndicator("KNU")
                .setContent(R.id.forth) );
        
        
        host.setCurrentTab(0);
        
        LinearLayout context = (LinearLayout)findViewById(R.id.first);
        registerForContextMenu(context);
        
	}
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.NONE, Menu.FIRST+1, Menu.NONE, "옵션1");
        menu.add(Menu.NONE, Menu.FIRST+2, Menu.NONE, "옵션2");
        menu.add(Menu.NONE, Menu.FIRST+3, Menu.NONE, "옵션3");
        menu.add(Menu.NONE, Menu.FIRST+4, Menu.NONE, "옵션4");
        return (super.onCreateOptionsMenu(menu));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return (onOption(item) || super.onOptionsItemSelected(item));
    }
    private boolean onOption(MenuItem item) {
        Toast msg = null;

        switch (item.getItemId()) {
        case Menu.FIRST+1:
            msg = Toast.makeText(this, "옵션1 선택", Toast.LENGTH_SHORT);
            msg.show();
            return true;

        case Menu.FIRST+2:
            msg = Toast.makeText(this, "옵션2 선택", Toast.LENGTH_SHORT);
            msg.show();
            return true;

        case Menu.FIRST+3:
            msg = Toast.makeText(this, "옵션3 선택", Toast.LENGTH_SHORT);
            msg.show();
            return true;
        case Menu.FIRST+4:
            msg = Toast.makeText(this, "옵션3 선택", Toast.LENGTH_SHORT);
            msg.show();
            return true;    
            
            
        }
        return false;
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu,
            View v, ContextMenuInfo info) {
        super.onCreateContextMenu(menu, v, info);
        menu.setHeaderTitle("어떤 로고가 마음에 드십니까?");
        menu.add(0, v.getId(), 0, "첫 번째");
        menu.add(0, v.getId(), 0, "두 번째");
        menu.add(0, v.getId(), 0, "세 번째");
        menu.add(0, v.getId(), 0, "네 번째");
    }
    
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Toast msg = Toast.makeText(this, item.getTitle() + "가 마음에 듭니다.", Toast.LENGTH_SHORT);
        msg.show();
        return true;
    }

}
